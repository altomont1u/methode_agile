package main;

public class TacheUtil {
	
	private int raf;
	private int estime;
	private int consomme;
	
	public int getRaf() {
		return raf;
	}

	
	public TacheUtil(int e, int c){
		this.estime = e;
		this.consomme = c;
	}
	
	public int calculRAF(){
		this.raf = this.estime - this.consomme;
		return this.raf;
	}
	
	public double calculEcart(){
		return (double)(calculRAF())/(this.estime*100) ;
	}
	
	public void sleep(int duree) throws InterruptedException{
		Thread.sleep(duree);
	}
	
	public boolean exists(){
		return true;
	}
}

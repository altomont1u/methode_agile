package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.After;
import org.junit.Test;


import main.TacheUtil;

public class TacheUtilTest {

	@Test
	public void soustractionEntiers() {
		final TacheUtil tache = new TacheUtil(8,3);
		Assert.assertEquals(5, tache.calculRAF());
	}
	
	@Test
	public void calculEcart(){
		final TacheUtil tache = new TacheUtil(12,3);
		Assert.assertEquals(0.0075,tache.calculEcart(),0);
	}
	
	@Test(timeout=1000)
    public void dureeRespectee() throws InterruptedException {
		final TacheUtil tache = new TacheUtil(12,3);
		tache.sleep(500);
    }

    @Test(timeout=1000)
    public void dureeNonRespectee() throws InterruptedException {
    	final TacheUtil tache = new TacheUtil(12,3);
        tache.sleep(2000);
    }
    
    @Before
    public void avant(){
    	System.out.println("Début du test...");
    }
    
    @After
    public void apres(){
    	System.out.println("Test terminé !");
    }
    
    @Ignore
    @Test
    public void ignore(){
    	System.out.println("Ceci ne doit pas s'afficher");
    }
    
    @Test
    public void exist(){
    	final TacheUtil tache = new TacheUtil(12,3);
    	Assert.assertTrue("La tâche existe !", tache.exists());
    }
    
    @Test
    public void nul(){
    	final TacheUtil tache = null;
    	Assert.assertNull("La tâche est nulle !",tache);
    }

}
